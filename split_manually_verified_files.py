import os
import glob
import shutil
import pandas as pd

train = pd.read_csv("train.csv")
verified_df = train[train['manually_verified'] == 1]

sound_names = verified_df['label'].unique().tolist()
# labels_as_num = {label: i for i, label in enumerate(sound_names, 0)}

labels_as_num = {'Saxophone'            :  0,
                 'Glockenspiel'         :  1,
                 'Cello'                :  2,
                 'Knock'                :  3,
                 'Gunshot_or_gunfire'   :  4,
                 'Hi-hat'               :  5,
                 'Laughter'             :  6,
                 'Flute'                :  7,
                 'Telephone'            :  8,
                 'Bark'                 :  9,
                 'Scissors'             : 10,
                 'Gong'                 : 11,
                 'Microwave_oven'       : 12,
                 'Shatter'              : 13,
                 'Harmonica'            : 14,
                 'Bass_drum'            : 15,
                 'Oboe'                 : 16,
                 'Bus'                  : 17,
                 'Tambourine'           : 18,
                 'Keys_jangling'        : 19,
                 'Electric_piano'       : 20,
                 'Clarinet'             : 21,
                 'Fireworks'            : 22,
                 'Meow'                 : 23,
                 'Double_bass'          : 24,
                 'Cough'                : 25,
                 'Acoustic_guitar'      : 26,
                 'Violin_or_fiddle'     : 27,
                 'Snare_drum'           : 28,
                 'Squeak'               : 29,
                 'Finger_snapping'      : 30,
                 'Writing'              : 31,
                 'Trumpet'              : 32,
                 'Drawer_open_or_close' : 33,
                 'Cowbell'              : 34,
                 'Tearing'              : 35,
                 'Fart'                 : 36,
                 'Chime'                : 37,
                 'Burping_or_eructation': 38,
                 'Computer_keyboard'    : 39,
                 'Applause'             : 40
                 }

# path = 'audio_train/manually_verified/'
# folder = os.listdir(path)

# filenames = set(verified_df['fname'].tolist())
# if not os.path.exists(path + 'manually_verified/'):
#     os.mkdir(path + 'manually_verified/')

# if not os.path.exists(path + 'not_verified/'):
#     os.mkdir(path + 'not_verified/')

# for file in folder:
#     if file in filenames:
#         shutil.move(path + file, path + 'manually_verified/')
#     else:
#         shutil.move(path + file, path + 'not_verified/')

# train.loc['00044347.wav']

# for file in folder:
#     label = verified_df.loc[verified_df['fname'] == file]['label'].tolist()[0]
#     new_name =  file[:8] + '-' + str(labels_as_num[label]) + '.wav'
#     os.rename(path + file, path + new_name)
        
# for row in train:
# category_group = train.groupby(['label', 'manually_verified'])
# print(category_group.to_string())