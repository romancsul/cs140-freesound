# import numpy as np
# np.random.seed(1001)

# import os
# import shutil

# import IPython
# import matplotlib
# import matplotlib.pyplot as plt
# import pandas as pd

# import seaborn as sns
# from tqdm import tqdm_notebook
# from sklearn.cross_validation import StratifiedKFold
# matplotlib inline
import glob
import os
import matplotlib.pyplot as plt
import numpy as np
from scipy import signal
from scipy.io import wavfile
import librosa
import time
from tqdm import tqdm, trange
import io
import json
import tensorflow as tf
from tensorflow.python.ops import rnn, rnn_cell

np.set_printoptions(threshold=np.inf)

# def extract_feature(file_name):
#     X, sample_rate = librosa.load(file_name)
#     # Short Fourier Transform
#     stft     = np.abs(librosa.stft(X))
    
#     # Mel- frequency cepstrum
#     mfccs    = np.mean(librosa.feature.mfcc(             y = X                          , sr = sample_rate, n_mfcc = 40).T      , axis = 0)
    
#     # Chromagram
#     chroma   = np.mean(librosa.feature.chroma_stft(      S = stft                       , sr = sample_rate).T                   , axis = 0)
    
#     # Mel-scaled power spectrogram
#     mel      = np.mean(librosa.feature.melspectrogram(   X                              , sr = sample_rate).T                   , axis = 0)
    
#     # Spectral contrast
#     contrast = np.mean(librosa.feature.spectral_contrast(S = stft                       , sr = sample_rate).T                   , axis = 0)
    
#     # Tonal centroid features (tonnetz)
#     tonnetz  = np.mean(librosa.feature.tonnetz(          y = librosa.effects.harmonic(X), sr = sample_rate).T                   , axis = 0)
    
#     return mfccs, chroma, mel, contrast, tonnetz

# def parse_audio_files(file_ext='*.wav'):
#     features, labels = np.empty((0 , 193)), np.empty(0)
    
#     for filename in tqdm(glob.glob(os.path.join('audio_train', 'manually_verified', file_ext))):
#         # mfccs,chroma,mel,contrast,tonnetz = extract_feature(filename)   
#         # ext_features = np.hstack( [mfccs, chroma, mel, contrast, tonnetz] )
#         # features     = np.vstack( [features, ext_features] )

#         labels       = np.append(labels, filename.split('-')[1][:-4])
    
#     return(np.array(features), np.array(labels, dtype = np.int))

def one_hot_encode(labels):
    """
    Turns numerical-categorical data into binary classification data
    """

    n_labels = len(labels)
    n_unique_labels = len(np.unique(labels))
    one_hot_encode = np.zeros((n_labels, n_unique_labels))
    one_hot_encode[np.arange(n_labels), labels] = 1
    return one_hot_encode

# Runs MFCC, CHROMA, MEL, CONTRAST and TONNETZ on all audio file,
# only need to run once
# all_features, all_labels = parse_audio_files()


# all_labels = one_hot_encode(all_labels)
# np.save('feature_memfile.npy', all_features)
# np.save('labels_memfile.npy', all_labels)

# all_features = np.load('feature_memfile.npy')
# all_labels   = np.load('labels_memfile.npy')

# train_test_split = np.random.rand(len(all_features)) < 0.70
# train_x = all_features[train_test_split]
# train_y = all_labels[train_test_split]
# test_x  = all_features[~train_test_split]
# test_y  = all_labels[~train_test_split]

# training_epochs    = 50
# n_dim              = all_features.shape[1]
# n_classes          = 41
# n_hidden_units_one = 280
# n_hidden_units_two = 300
# standard_deviation = 1 / np.sqrt(n_dim)
# learning_rate      = 0.001

# X = tf.placeholder(tf.float32,[None, n_dim])
# Y = tf.placeholder(tf.float32,[None, n_classes])

# #First Layer
# W_1 = tf.Variable(tf.random_normal([n_dim, n_hidden_units_one], mean = 0, stddev = standard_deviation))
# b_1 = tf.Variable(tf.random_normal([n_hidden_units_one]       , mean = 0, stddev = standard_deviation))
# h_1 = tf.nn.tanh(tf.matmul(X, W_1) + b_1)

# #Second Layer
# W_2 = tf.Variable(tf.random_normal([n_hidden_units_one, n_hidden_units_two], mean = 0, stddev = standard_deviation))
# b_2 = tf.Variable(tf.random_normal([n_hidden_units_two]                    , mean = 0, stddev = standard_deviation))
# h_2 = tf.nn.sigmoid(tf.matmul(h_1, W_2) + b_2)

# W  = tf.Variable(tf.random_normal([n_hidden_units_two, n_classes], mean = 0, stddev = standard_deviation))
# b  = tf.Variable(tf.random_normal([n_classes]                    , mean = 0, stddev = standard_deviation))
# y_ = tf.nn.softmax(tf.matmul(h_2, W) + b)

# # init = tf.initialize_all_variables()
# init = tf.global_variables_initializer()

# # cost_function = tf.reduce_sum(Y * tf.log(y_))
# cost_function = tf.reduce_mean(-tf.reduce_sum(Y * tf.log(y_), reduction_indices=[1]))
# optimizer     = tf.train.GradientDescentOptimizer(learning_rate).minimize(cost_function)

# correct_prediction = tf.equal(tf.argmax(y_, 1), tf.argmax(Y, 1))
# accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

# cost_history = np.empty(shape = [1], dtype = float)
# y_true, y_pred = None, None

# with tf.Session() as sess:
#     sess.run(init)
#     for epoch in range(training_epochs):
#         _, cost = sess.run([optimizer, cost_function], feed_dict = {X:train_x, Y:train_y})
#         cost_history = np.append(cost_history, cost)

#     y_pred = sess.run(tf.argmax(y_,1),feed_dict={X: test_x})
#     y_true = sess.run(tf.argmax(test_y,1))
    
#     print("Test accuracy: ",round(sess.run(accuracy, 
#         feed_dict = {X: test_x,Y: test_y}),3))

def windows(data, window_size):
    """
    Windows are essentially sliding scales used to to evaluate different slices of the 
    original file with overlap. 

    Needs to be adjusted so that it does not unevenly slice files. With this code it will not capture 
    the final portion of the audio files evenly
    """
    start = 0
    while start < len(data):
        yield start, start + window_size
        start += int(window_size / 2)

def extract_features(bands = 20, frames = 41):
    window_size = 512 * (frames - 1)
    mfccs  = []
    labels = []


    """
        The files should be structure with the 'filename-label.wav' There is a helper function 
        that does this. TQDM gives us a progress bar
    """
    for filename in tqdm(glob.glob(os.path.join('audio_train', 'manually_verified', '*.wav'))):
        sound_clip, sample_rate = librosa.load(filename)
        
        label = filename.split('-')[1][:-4]
        
        # Performs MFCC on all slices of the audio file and then flattens them in a single 1D array
        for (start, end) in windows(sound_clip, window_size):
            signal = sound_clip[start:end]
            mfcc = librosa.feature.mfcc(y = signal, sr = sample_rate, n_mfcc = bands).T.flatten()[:, np.newaxis].T.flatten()
            mfccs.append(mfcc)
            labels.append(label)
    
    features = np.asarray(mfccs).reshape(len(mfccs), frames, bands)

    return np.array(features), np.array(labels, dtype = np.int)

all_features, all_labels = extract_features()

learning_rate = 0.01
training_iterations = 1000
batch_size = 50
displaty_step = 200

n_input = 20
n_steps = 41
n_hidden = 300
n_classes = 10

x = tf.placeholder("float", [None, n_steps, n_input])
y = tf.placeholder("float", [None, n_classes])

weight = tf.Variable(tf.random_normal([n_hidden, n_classes]))
bias   = tf.Variable(tf.random_normal([n_classes]))


